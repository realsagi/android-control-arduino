#include <SPI.h>
#include <WiFi.h>
#include <EEPROM.h>
#include <DS3232RTC.h>
#include <Wire.h>
#include <Time.h> 
#include <Streaming.h>

IPAddress ip(192, 168, 1, 151); 
int status = WL_IDLE_STATUS;
char ssid[] = "ELEC-2014";  //  your network SSID (name)
char pass[] = "kom-2014";       // your network password
int keyIndex = 0;            // your network key Index number (needed only for WEP)

int keyavailable = 0;
String time = "";
int hourc = 0;
int minut = 0;
String ch1,ch2,ch3,ch4;
String ho1,mo1,hf1,mf1;
String ho2,mo2,hf2,mf2;
String ho3,mo3,hf3,mf3;
String ho4,mo4,hf4,mf4;

WiFiServer server(23);

void setup()
{
  WiFi.config(ip);
  // Open serial communications and wait for port to open:
  Serial.begin(9600);

  // check for the presence of the shield:
  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("WiFi shield not present");
    // don't continue:
    while (true);
  }

  String fv = WiFi.firmwareVersion();
  if ( fv != "1.1.0" )
    Serial.println("Please upgrade the firmware");
  // attempt to connect to Wifi network:
  while ( status != WL_CONNECTED) {
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(ssid);
    // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
    status = WiFi.begin(ssid, pass);
    // wait 10 seconds for connection:
    delay(10000);
    server.begin();
  }
  setSyncProvider(RTC.get);  
  
  if(timeStatus() != timeSet) 
    Serial.println("Unable to sync with the RTC");
  else
    Serial.println("RTC has set the system time");  
    
  Serial << F("RTC Sync");
  
  if (timeStatus() != timeSet) Serial << F(" FAIL!");
    Serial << endl;

  Serial.println("Connected to wifi");
  printWifiStatus();
  
  pinMode(A0,OUTPUT);
  pinMode(A1,OUTPUT);
  pinMode(A2,OUTPUT);
  pinMode(A3,OUTPUT);
  pinMode(2,OUTPUT);
  
  showstate();
}

void loop()
{
  digitalWrite(2,LOW);  
  // wait for a new client:
  static time_t tLast;
  time_t t;
  tmElements_t tm;
  if (Serial.available() >= 12) {
        //note that the tmElements_t Year member is an offset from 1970,
        //but the RTC wants the last two digits of the calendar year.
        //use the convenience macros from Time.h to do the conversions.
        int y = Serial.parseInt();
        if (y >= 100 && y < 1000)
            Serial << F("Error: Year must be two digits or four digits!") << endl;
        else {
            if (y >= 1000)
                tm.Year = CalendarYrToTm(y);
            else    //(y < 100)
                tm.Year = y2kYearToTm(y);
            tm.Month = Serial.parseInt();
            tm.Day = Serial.parseInt();
            tm.Hour = Serial.parseInt();
            tm.Minute = Serial.parseInt();
            tm.Second = Serial.parseInt();
            t = makeTime(tm);
            RTC.write(tm);
            setTime(t);        
            Serial << F("RTC set to: ");
            printDateTime(t);
            Serial << endl;
            //dump any extraneous input
            while (Serial.available() > 0) Serial.read();
        }
  }
  WiFiClient client = server.available();
  if (client) {
    String clientMsg ="";
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        //Serial.print(c);
        clientMsg+=c;//store the recieved chracters in a string
        //if the character is an "end of line" the whole message is recieved

        if (c == '\n') {
          Serial.println(clientMsg);//print it to the serial
          if(clientMsg == "showstate\r\n"){
            showstate(); 
          }
          else if(clientMsg == "showstatetime\r\n"){
            timmer(); 
            timmer2();
          }
          else if(clientMsg == "open1\r\n"){
            digitalWrite(A0,HIGH);
            client.println("LED 1 Open");
            EEPROM.write(16,1); // write on state LED 1
          }
          else if(clientMsg == "close1\r\n"){
            digitalWrite(A0,LOW);
            client.println("LED 1 Close");;
            EEPROM.write(16,0); // write off state LED 1
          }
          else if(clientMsg == "open2\r\n"){
            digitalWrite(A1,HIGH);
            client.println("LED 2 Open");
            EEPROM.write(17,1); // write on state LED 2
          }
          else if(clientMsg == "close2\r\n"){
            digitalWrite(A1,LOW);
            client.println("LED 2 Close");
            EEPROM.write(17,0); // write on state LED 2
          }
          else if(clientMsg == "open3\r\n"){
            digitalWrite(A2,HIGH);
            client.println("LED 3 Open");
            EEPROM.write(18,1); // write on state LED 3
          }
          else if(clientMsg == "close3\r\n"){
            digitalWrite(A2,LOW);
            client.println("LED 3 Close");
            EEPROM.write(18,0); // write on state LED 3
          }
          else if(clientMsg == "open4\r\n"){
            digitalWrite(A3,HIGH);
            client.println("LED 4 Open");
            EEPROM.write(19,1); // write on state LED 4
          }
          else if(clientMsg == "close4\r\n"){
            digitalWrite(A3,LOW);
            client.println("LED 4 Close");
            EEPROM.write(19,0); // write on state LED 4
          }
          else if(clientMsg == "Set_time_ch1\r\n"){
            waitesettime(0);
          }
          else if(clientMsg == "Set_time_ch2\r\n"){
            waitesettime(4);
          }
          else if(clientMsg == "Set_time_ch3\r\n"){
            waitesettime(8);
          }
          else if(clientMsg == "Set_time_ch4\r\n"){
            waitesettime(12);
          }
          clientMsg="";
        }
      }
    }
    // give the Client time to receive the data
    delay(1);
    // close the connection:
    client.stop();
  }
     Serial.print(hour());
     Serial.print(' ');
     Serial.print(minute());
     Serial.print(' ');
     Serial.print(second());
     Serial.print(' ');
     Serial.print(day());
     Serial.print(' ');
     Serial.print(month());
     Serial.print(' ');
     Serial.print(year());
     Serial.println(); 
   hourc = hour();
   minut = minute();
   Serial.println((hourc * 100) + minut);
   delay(1000);
   if(((hourc * 100) + minut == (EEPROM.read(0) * 100) + EEPROM.read(1))){
     digitalWrite(A0,HIGH);
     EEPROM.write(16,1); // write on state LED 1
   }
   if(((hourc * 100) + minut == (EEPROM.read(2) * 100) + EEPROM.read(3))){
     digitalWrite(A0,LOW);
     EEPROM.write(16,0); // write on state LED 1
   }
   if(((hourc * 100) + minut == (EEPROM.read(4) * 100) + EEPROM.read(5))){
     digitalWrite(A1,HIGH);
     EEPROM.write(17,1); // write on state LED 2
   }
   if(((hourc * 100) + minut == (EEPROM.read(6) * 100) + EEPROM.read(7))){
     digitalWrite(A1,LOW);
     EEPROM.write(17,0); // write on state LED 2
   }
   if(((hourc * 100) + minut == (EEPROM.read(8) * 100) + EEPROM.read(9))){
     digitalWrite(A2,HIGH);
     EEPROM.write(18,1); // write on state LED 3
   }
   if(((hourc * 100) + minut == (EEPROM.read(10) * 100) + EEPROM.read(11))){
     digitalWrite(A2,LOW);
     EEPROM.write(18,0); // write on state LED 3
   }
   if(((hourc * 100) + minut == (EEPROM.read(12) * 100) + EEPROM.read(13))){
     digitalWrite(A3,HIGH);
     EEPROM.write(19,1); // write on state LED 4
   }
   if(((hourc * 100) + minut == (EEPROM.read(14) * 100) + EEPROM.read(15))){
     digitalWrite(A3,LOW);
     EEPROM.write(19,0); // write on state LED 4
   }
}
void printDate(time_t t)
{
    printI00(day(t), 0);
    Serial << monthShortStr(month(t)) << _DEC(year(t));
}
void printDateTime(time_t t)
{
    printDate(t);
    Serial << ' ';
    printTime(t);
}
void printTime(time_t t)
{
    printI00(hour(t), ':');
    printI00(minute(t), ':');
    printI00(second(t), ' ');
}
void printI00(int val, char delim)
{
    if (val < 10) Serial << '0';
    Serial << _DEC(val);
    if (delim > 0) Serial << delim;
    return;
}

void waitesettime(int a){
  digitalWrite(2,LOW);
  // wait for a new client:
  WiFiClient client = server.available();
  if (client) {
    String clientMsg ="";
    while (client.connected()) {
      digitalWrite(2,HIGH);
      if (client.available()) {
        char c = client.read();
        //Serial.print(c);
        clientMsg+=c;//store the recieved chracters in a string
        //if the character is an "end of line" the whole message is recieved

        if (c == '\n') {
          Serial.println(clientMsg.substring(0,5));
          if(clientMsg.substring(0,5) == "h_on-"){
            Serial.println(clientMsg.substring(5,7));
            EEPROM.write(a, clientMsg.substring(5,7).toInt());
          }
          if(clientMsg.substring(0,5) == "m_on-"){
            Serial.println(clientMsg.substring(5,7));
            EEPROM.write(a+1, clientMsg.substring(5,7).toInt());
          }
          if(clientMsg.substring(0,6) == "h_off-"){
            Serial.println(clientMsg.substring(6,8));
            EEPROM.write(a+2, clientMsg.substring(6,8).toInt());
          }
          if(clientMsg.substring(0,6) == "m_off-"){
            Serial.println(clientMsg.substring(6,8));
            EEPROM.write(a+3, clientMsg.substring(6,8).toInt());
          }
          if(clientMsg == "save\r\n"){
            break; 
          }
          clientMsg="";
        }
      }
    }
    // give the Client time to receive the data
    delay(1);
  }
}

void timmer(){
      ho1 = "ho1-"+String(EEPROM.read(0));
      mo1 = "mo1-"+String(EEPROM.read(1));
      hf1 = "hf1-"+String(EEPROM.read(2));
      mf1 = "mf1-"+String(EEPROM.read(3));
      
      ho2 = "ho2-"+String(EEPROM.read(4));
      mo2 = "mo2-"+String(EEPROM.read(5));
      hf2 = "hf2-"+String(EEPROM.read(6));
      mf2 = "mf2-"+String(EEPROM.read(7));
    
  
     server.println(ho1);
     server.println(mo1);
     server.println(hf1);
     server.println(mf1);
     
     server.println(ho2);
     server.println(mo2);  
     server.println(hf2);
     server.println(mf2);
     
     
     Serial.println(ho1);
     Serial.println(mo1);
     Serial.println(hf1);
     Serial.println(mf1);
     Serial.println(ho2);
     Serial.println(mo2);
     Serial.println(hf2);
     Serial.println(mf2);
}
void timmer2(){
      ho3 = "ho3-"+String(EEPROM.read(8));
      mo3 = "mo3-"+String(EEPROM.read(9));
      hf3 = "hf3-"+String(EEPROM.read(10));
      mf3 = "mf3-"+String(EEPROM.read(11));
      
      ho4 = "ho4-"+String(EEPROM.read(12));
      mo4 = "mo4-"+String(EEPROM.read(13));
      hf4 = "hf4-"+String(EEPROM.read(14));
      mf4 = "mf4-"+String(EEPROM.read(15));
      
     server.println(ho3);
     server.println(mo3);
     server.println(hf3);
     server.println(mf3);
     
     server.println(ho4);
     server.println(mo4);
     server.println(hf4);
     server.println(mf4);
     
     server.println("endshowtimmer"); 
     
     Serial.println(ho3);
     Serial.println(mo3);
     Serial.println(hf3);
     Serial.println(mf3);
     Serial.println(ho4);
     Serial.println(mo4);
     Serial.println(hf4);
     Serial.println(mf4);
     Serial.println("endshowtimmer");
}

void showstate(){   
    ch1 = String(EEPROM.read(16));
    ch2 = String(EEPROM.read(17));
    ch3 = String(EEPROM.read(18));
    ch4 = String(EEPROM.read(19));
    if(ch1.equals("1")){
      server.println("LED 1 Open");
      Serial.println("LED 1 Open");
      digitalWrite(A0,HIGH);
    }
    if(ch1.equals("0")){
      server.println("LED 1 Close");
      Serial.println("LED 1 Close");
      digitalWrite(A0,LOW);
    }
    if(ch2.equals("1")){
      server.println("LED 2 Open");
      Serial.println("LED 2 Open");
      digitalWrite(A1,HIGH);
    }
    if(ch2.equals("0")){
      server.println("LED 2 Close");
      Serial.println("LED 2 Close");
      digitalWrite(A1,LOW);
    }
    if(ch3.equals("1")){
      server.println("LED 3 Open");
      Serial.println("LED 3 Open");
      digitalWrite(A2,HIGH);
    }
    if(ch3.equals("0")){
      server.println("LED 3 Close");
      Serial.println("LED 3 Close");
      digitalWrite(A2,LOW);
    }
    if(ch4.equals("1")){
      server.println("LED 4 Open");
      Serial.println("LED 4 Open");
      digitalWrite(A3,HIGH);
    }
    if(ch4.equals("0")){
      server.println("LED 4 Close");
      Serial.println("LED 4 Close");
      digitalWrite(A3,LOW);
    }
    digitalWrite(2,HIGH);
    server.println("endshowstate");
    Serial.println("endshowstate");
}

void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}
