package com.siliku.mobilecontrol;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class ClientThread implements Runnable {

    private Socket socket;

    private static int SERVERPORT;
    private static String SERVER_IP;

    public String readline = " ";
    private BufferedReader input;
    public String ho1,mo1,hf1,mf1;
    public String ho2,mo2,hf2,mf2;
    public String ho3,mo3,hf3,mf3;
    public String ho4,mo4,hf4,mf4;
    public String led1,led2,led3,led4;
    public String endstate = "null";

    @Override
    public void run() {

        try {
            System.out.println(SERVER_IP);
            System.out.println(SERVERPORT);
            InetAddress serverAddr = InetAddress.getByName(SERVER_IP);

            socket = new Socket(serverAddr, SERVERPORT);

            this.input = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));

        } catch (UnknownHostException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        while(!Thread.currentThread().isInterrupted()){
            try{
                readline = input.readLine();
                if(readline.substring(0,4).equals("ho1-")){
                    System.out.println(readline.substring(4));
                    ho1 = readline.substring(4);
                }
                else if(readline.substring(0,4).equals("mo1-")){
                    System.out.println(readline.substring(4));
                    mo1 = readline.substring(4);
                }
                else if(readline.substring(0,4).equals("hf1-")){
                    System.out.println(readline.substring(4));
                    hf1 = readline.substring(4);
                }
                else if(readline.substring(0,4).equals("mf1-")){
                    System.out.println(readline.substring(4));
                    mf1 = readline.substring(4);
                }

                else if(readline.substring(0,4).equals("ho2-")){
                    System.out.println(readline.substring(4));
                    ho2 = readline.substring(4);
                }
                else if(readline.substring(0,4).equals("mo2-")){
                    System.out.println(readline.substring(4));
                    mo2 = readline.substring(4);
                }
                else if(readline.substring(0,4).equals("hf2-")){
                    System.out.println(readline.substring(4));
                    hf2 = readline.substring(4);
                }
                else if(readline.substring(0,4).equals("mf2-")){
                    System.out.println(readline.substring(4));
                    mf2 = readline.substring(4);
                }

                else if(readline.substring(0,4).equals("ho3-")){
                    System.out.println(readline.substring(4));
                    ho3 = readline.substring(4);
                }
                else if(readline.substring(0,4).equals("mo3-")){
                    System.out.println(readline.substring(4));
                    mo3 = readline.substring(4);
                }
                else if(readline.substring(0,4).equals("hf3-")){
                    System.out.println(readline.substring(4));
                    hf3 = readline.substring(4);
                }
                else if(readline.substring(0,4).equals("mf3-")){
                    System.out.println(readline.substring(4));
                    mf3 = readline.substring(4);
                }

                else if(readline.substring(0,4).equals("ho4-")){
                    System.out.println(readline.substring(4));
                    ho4 = readline.substring(4);
                }
                else if(readline.substring(0,4).equals("mo4-")){
                    System.out.println(readline.substring(4));
                    mo4 = readline.substring(4);
                }
                else if(readline.substring(0,4).equals("hf4-")){
                    System.out.println(readline.substring(4));
                    hf4 = readline.substring(4);
                }
                else if(readline.substring(0,4).equals("mf4-")){
                    System.out.println(readline.substring(4));
                    mf4 = readline.substring(4);
                }
                else if(readline.substring(0,5).equals("LED 1")){
                    System.out.println(readline.substring(6));
                    led1 = getReadline().substring(6);
                }
                else if(readline.substring(0,5).equals("LED 2")){
                    System.out.println(readline.substring(6));
                    led2 = getReadline().substring(6);
                }
                else if(readline.substring(0,5).equals("LED 3")){
                    System.out.println(readline.substring(6));
                    led3 = getReadline().substring(6);
                }
                else if(readline.substring(0,5).equals("LED 4")){
                    System.out.println(readline.substring(6));
                    led4 = getReadline().substring(6);
                }
                else{
                    System.out.println(readline);
                    endstate = readline;
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    public void set_server_ip(String ipserver){
        SERVER_IP = ipserver;
    }

    public void set_server_port(int serverport){
        SERVERPORT = serverport;
    }

    public void sentdata(String data){
        try {
            String str = data;
            PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())),true);
            out.println(str);
        }
        catch(UnknownHostException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void disconnect(){
        try {
            socket.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }

    public String getend(){
        return endstate;
    }

    public String getReadline(){
        return readline;
    }
}
