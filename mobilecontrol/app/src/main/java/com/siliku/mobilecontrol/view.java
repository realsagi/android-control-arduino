package com.siliku.mobilecontrol;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.concurrent.ExecutionException;

public class view extends ActionBarActivity {

    private Boolean checkcn1 = false;
    private Boolean checkcn2 = false;
    private Boolean checkcn3 = false;
    private Boolean checkcn4 = false;
    ClientThread client = new ClientThread();
    Thread thread = new Thread(client);
    private String statussavetime = "";
    private String text_hour_on = "";
    private String text_min_on = "";
    private String text_hour_off = "";
    private String text_min_off = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try{
            Thread.sleep(2000);
        }catch (Exception e){
            e.printStackTrace();
        }
        showactivity_view();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showactivity_view(){
        setContentView(R.layout.activity_view);

        //edit text Ip address
        final EditText editipaddress = (EditText)findViewById(R.id.ipadd);
        //edit text Port
        final EditText editport = (EditText)findViewById(R.id.port);
        //button connect
        final Button buttonconnect = (Button)findViewById(R.id.connect);

        readwritefile data = new readwritefile();
        editipaddress.setText(data.read("ipaddress"));
        editport.setText(data.read("port"));

        buttonconnect.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                try {
                    readwritefile data = new readwritefile();
                    data.write("ipaddress",editipaddress.getText().toString());
                    if(data.write("ipaddress", editipaddress.getText().toString())){
                        System.out.println("ipaddress save complete");
                    }else{
                        System.out.println("I/O ERROR");
                    }

                    data.write("port",editport.getText().toString());
                    if(data.write("port", editport.getText().toString())){
                        System.out.println("port save complete");
                    }else{
                        System.out.println("I/O ERROR");
                    }

                    int portnum = Integer.parseInt(editport.getText().toString());
                    new ClientThread().set_server_ip(editipaddress.getText().toString());
                    new ClientThread().set_server_port(portnum);
                    thread.start();
                    waiteconnect();
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    public void waiteconnect(){
        try {
            setContentView(R.layout.p_waite);
            final Button go1 = (Button) findViewById(R.id.btgo);
            go1.setEnabled(false);
            Thread.sleep(2000);
            client.sentdata("showstate\r\n");
            while(true){
                if (client.readline.equals("endshowstate")) {
                    go1.setEnabled(true);
                    break;
                }
            }
            go1.setOnClickListener(new Button.OnClickListener() {
                public void onClick(View v) {
                    client.sentdata("showstatetime\r\n");
                    while(true){
                        if(client.readline.equals("endshowtimmer")){
                            break;
                        }
                    }
                    conArduino();
                }
            });
        }
        catch (Exception e){
            System.out.println(e);
        }
    }

    public void conArduino(){
        setContentView(R.layout.mainprogram);
        try {
            //-------------------------------------------------------
            // button cn1
            final Button cn1 = (Button) findViewById(R.id.led1);
            // button st1
            final Button st1 = (Button) findViewById(R.id.settime1);
            //--------------------------------------------------------

            //--------------------------------------------------------
            // button cn2
            final Button cn2 = (Button) findViewById(R.id.led2);
            // button st2
            final Button st2 = (Button) findViewById(R.id.settime2);
            //--------------------------------------------------------

            //--------------------------------------------------------
            // button cn3
            final Button cn3 = (Button) findViewById(R.id.led3);
            // button st2
            final Button st3 = (Button) findViewById(R.id.settime3);
            //--------------------------------------------------------

            //--------------------------------------------------------
            // button cn4
            final Button cn4 = (Button) findViewById(R.id.led4);
            // button st2
            final Button st4 = (Button) findViewById(R.id.settime4);
            //--------------------------------------------------------

            if(client.led1.equals("Open")){
                cn1.setText("Channel1_ON");
                checkcn1 = true;
            }
            if(client.led1.equals("Close")){
                cn1.setText("Channel1_CLOSE");
                checkcn1 = false;
            }

            if(client.led2.equals("Open")){
                cn2.setText("Channel2_ON");
                checkcn2 = true;
            }
            if(client.led2.equals("Close")){
                cn2.setText("Channel2_CLOSE");
                checkcn2 = false;
            }

            if(client.led3.equals("Open")){
                cn3.setText("Channel3_ON");
                checkcn3 = true;
            }
            if(client.led3.equals("Close")){
                cn3.setText("Channel3_CLOSE");
                checkcn3 = false;
            }

            if(client.led4.equals("Open")){
                cn4.setText("Channel4_ON");
                checkcn4 = true;
            }
            if(client.led4.equals("Close")){
                cn4.setText("Channel4_CLOSE");
                checkcn4 = false;
            }

            // button dis con
            final Button discon = (Button) findViewById(R.id.discon);

            discon.setOnClickListener(new Button.OnClickListener() {
                public void onClick(View v) {
                    try {
                        System.exit(0);
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
            });

            cn1.setOnClickListener(new Button.OnClickListener() {
                public void onClick(View v) {
                    if (checkcn1) {
                        client.sentdata("close1\r\n");
                        cn1.setText("Channel1_CLOSE");
                        checkcn1 = false;
                    }else{
                        client.sentdata("open1\r\n");
                        cn1.setText("Channel1_ON");
                        checkcn1 = true;
                    }
                }
            });
            cn2.setOnClickListener((new Button.OnClickListener(){
                public void onClick(View v){
                    if (checkcn2) {
                        client.sentdata("close2\r\n");
                        cn2.setText("Channel2_CLOSE");
                        checkcn2 = false;
                    } else{
                        client.sentdata("open2\r\n");
                        cn2.setText("Channel2_ON");
                        checkcn2 = true;
                    }
                }
            }));
            cn3.setOnClickListener((new Button.OnClickListener(){
                public void onClick(View v){
                    if (checkcn3) {
                        client.sentdata("close3\r\n");
                        cn3.setText("Channel3_CLOSE");
                        checkcn3 = false;
                    } else{
                        client.sentdata("open3\r\n");
                        cn3.setText("Channel3_ON");
                        checkcn3 = true;
                    }
                }
            }));
            cn4.setOnClickListener((new Button.OnClickListener(){
                public void onClick(View v){
                    if (checkcn4) {
                        client.sentdata("close4\r\n");
                        cn4.setText("Channel4_CLOSE");
                        checkcn4 = false;
                    } else{
                        client.sentdata("open4\r\n");
                        cn4.setText("Channel4_ON");
                        checkcn4 = true;
                    }
                }
            }));
            st1.setOnClickListener(new Button.OnClickListener() {
                public void onClick(View v) {
                    showsettime("Set_time_ch1");
                }
            });
            st2.setOnClickListener(new Button.OnClickListener() {
                public void onClick(View v) {
                    showsettime("Set_time_ch2");
                }
            });
            st3.setOnClickListener(new Button.OnClickListener() {
                public void onClick(View v) {
                    showsettime("Set_time_ch3");
                }
            });
            st4.setOnClickListener(new Button.OnClickListener() {
                public void onClick(View v) {
                    showsettime("Set_time_ch4");
                }
            });
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void showsettime(String s){
        setContentView(R.layout.settime);

        TextView sthead = (TextView)findViewById(R.id.settimeh);
        sthead.setText(s);
        statussavetime = s;
        client.sentdata(statussavetime+"\r\n");

        //------------------------------------------------------
        //edit text hour on
        final EditText houron = (EditText)findViewById(R.id.hour1);
        //edit text minute on
        final EditText minuteon = (EditText)findViewById(R.id.minute1);
        //------------------------------------------------------

        //------------------------------------------------------
        //edit text hour off
        final EditText houroff = (EditText)findViewById(R.id.hour2);
        //edit text minute on
        final EditText minuteoff = (EditText)findViewById(R.id.minute2);
        //------------------------------------------------------

        if(statussavetime.equals("Set_time_ch1")){
            houron.setText(client.ho1);
            minuteon.setText(client.mo1);
            houroff.setText(client.hf1);
            minuteoff.setText(client.mf1);
        }
        else if(statussavetime.equals("Set_time_ch2")){
            houron.setText(client.ho2);
            minuteon.setText(client.mo2);
            houroff.setText(client.hf2);
            minuteoff.setText(client.mf2);
        }
        else if(statussavetime.equals("Set_time_ch3")){
            houron.setText(client.ho3);
            minuteon.setText(client.mo3);
            houroff.setText(client.hf3);
            minuteoff.setText(client.mf3);
        }
        else if(statussavetime.equals("Set_time_ch4")){
            houron.setText(client.ho4);
            minuteon.setText(client.mo4);
            houroff.setText(client.hf4);
            minuteoff.setText(client.mf4);
        }

        // button savesettim
        final Button savest = (Button)findViewById(R.id.savetime);

        houron.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //notnull
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //notnull
            }

            @Override
            public void afterTextChanged(Editable s) {
                text_hour_on = houron.getText().toString();
            }
        });

        minuteon.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //notnull
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //notnull
            }

            @Override
            public void afterTextChanged(Editable s) {
                text_min_on = minuteon.getText().toString();
            }
        });

        houroff.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //notnull
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //notnull
            }

            @Override
            public void afterTextChanged(Editable s) {
                text_hour_off = houroff.getText().toString();
            }
        });

        minuteoff.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //notnull
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //notnull
            }

            @Override
            public void afterTextChanged(Editable s) {
                text_min_off = minuteoff.getText().toString();
            }
        });
        savest.setOnClickListener(new Button.OnClickListener(){
           public void onClick(View v){
               client.sentdata("h_on-"+text_hour_on+"\r\n");
               client.sentdata("m_on-"+text_min_on+"\r\n");
               client.sentdata("h_off-"+text_hour_off+"\r\n");
               client.sentdata("m_off-"+text_min_off+"\r\n");
               client.sentdata("save\r\n");
               waiteconnect();
            }
        });
    }
}
